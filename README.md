# Simulador de vehículos de alta gama.

Integrantes:

*  Stefania Nieto
*  John Cruz
*  Jorge Pacheco

## Diagramas de caso de uso.

A continuación se documentan los casos de uso, definidos en el diagrama de general de casos de uso.
### Diagrama de caso de uso creación y modificación de un vehículo.
![Caso_de_uso_1](/uploads/5b9b97fcf5c0ed013dbd0cb215d216bf/Caso_de_uso_1.png)
![Captura_de_pantalla_2019-05-06_a_la_s__3.45.50_p._m.](/uploads/e11cb0ffc5e8b29fe37cba0b7a3e5dcd/Captura_de_pantalla_2019-05-06_a_la_s__3.45.50_p._m..png)
### Diagrama de caso de uso simulación del comportamiento de un vehículo.
![Captura_de_Pantalla_2019-05-20_a_la_s__3.13.10_p._m.](/uploads/6c64e352365f9d78ec2db1bb1cefef25/Captura_de_Pantalla_2019-05-20_a_la_s__3.13.10_p._m..png)
## Diagramas de actividades

El diagrama de actividades plasmado en la figura 5. Permite observar el flujo de trabajo que llevara a cabo el cliente al momento de realizar modificaciones para personalizar su vehículo. 

De acuerdo con el diagrama el usuario interactúa con la plataforma a través de una interfaz en la que podrá ir realizando cambios en el diseño del vehículo, que van desde el motor hasta cambios o modificaciones sencillas de acabados y accesorios. 

La interfaz permitirá al usuario visualizar los cambios realizados de manera gráfica, el usuario interactúa con la plataforma a través de botones que permiten desplegar las opciones conforme a un catálogo de sistemas modificables. 

Finalmente, el cliente hace una validación o verificación de la información modificada, la cual a su vez es validada por el sistema para efectuar los cambios en el diseño gráfico del vehículo. 
### Diagrama de actividades creación y modificación de un vehículo.
![Diagrama_de_actividades](/uploads/e6865b1e2792626c1b396b999ffb953f/Diagrama_de_actividades.png)
### Diagrama de actividades simulación del comportamiento de un vehículo.
![Captura_de_Pantalla_2019-05-20_a_la_s__3.19.39_p._m.](/uploads/d13f18c14bbcd43e913954264976eaf4/Captura_de_Pantalla_2019-05-20_a_la_s__3.19.39_p._m..png)


## Diagrama de secuencia

![PHOTO-2019-05-20-01-34-26](/uploads/ad2a92e70762669cf1f4462e22503ea9/PHOTO-2019-05-20-01-34-26.jpg)

## Diagrama de clases

### Diagrama de clases creación y modificación de un vehículo.

![Captura_de_Pantalla_2019-05-20_a_la_s__12.56.50_p._m.](/uploads/f519781e03a189cccd3953ea00842f4a/Captura_de_Pantalla_2019-05-20_a_la_s__12.56.50_p._m..png)

### Diagrama de clases con simulación de vehículos.

![Captura_de_Pantalla_2019-05-20_a_la_s__2.49.28_p._m.](/uploads/d8586e101331b218f1adcdf54e0d7059/Captura_de_Pantalla_2019-05-20_a_la_s__2.49.28_p._m..png)


